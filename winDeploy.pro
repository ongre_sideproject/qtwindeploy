QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
DESTDIR = ../_build

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

SOURCES += \
    backend.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    backend.h \
    mainwindow.h

FORMS += \
    mainwindow.ui
