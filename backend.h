#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QStringList>
#include <QProcess>

#include <QEventLoop>

class BackEnd : public QObject
{
    Q_OBJECT
public:
    explicit BackEnd(QObject *parent = nullptr);
    ~BackEnd();

signals:
    void    findQtDeployPath(QString path);
    void    standardOutput(QString str);
    void    standardError(QString str);


public slots:
    void    initThread();
    void    onDoProcess(QString deployPath, QStringList arg);


private slots:
    void	readyReadStandardError();
    void	readyReadStandardOutput();
    void    finished(int exitCode, QProcess::ExitStatus exitStatus);

private:
    QEventLoop * looper;
    QProcess *  m_process;


};

#endif // BACKEND_H
