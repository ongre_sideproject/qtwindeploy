#include "mainwindow.h"

#include <QApplication>
#include "backend.h"
#include <QThread>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    BackEnd backend;

    QObject::connect(&backend,SIGNAL(findQtDeployPath(QString)),&w,SLOT(onFindQtDeployPath(QString)));
    QObject::connect(&backend,SIGNAL(standardOutput(QString)),&w,SLOT(onStandardOutput(QString)));
    QObject::connect(&backend,SIGNAL(standardError(QString)),&w,SLOT(onStandardError(QString)));


    QObject::connect(&w,SIGNAL(doProcess(QString,QStringList)),&backend,SLOT(onDoProcess(QString,QStringList)));


    QThread * thread = new QThread;
    backend.moveToThread(thread);
    QObject::connect(thread,SIGNAL(started()),&backend,SLOT(initThread()));
    thread->start();

    w.show();
    return a.exec();
}
