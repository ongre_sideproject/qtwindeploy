#include "backend.h"
#include <QDebug>
#include <QDirIterator>
#include <QEventLoop>

BackEnd::BackEnd(QObject *parent) : QObject(parent)
{

}
BackEnd::~BackEnd()
{
    qDebug() << __FUNCTION__;
}
void BackEnd::initThread()
{
    looper = new QEventLoop();
    m_process = new QProcess(looper);
    m_process->setProcessChannelMode(QProcess::SeparateChannels);
    connect(m_process,SIGNAL(readyReadStandardOutput()),this,SLOT(readyReadStandardOutput()));
    connect(m_process,SIGNAL(readyReadStandardError()),this,SLOT(readyReadStandardError()));
    connect(m_process,SIGNAL(finished(int,QProcess::ExitStatus)),this,SLOT(finished(int,QProcess::ExitStatus)));

    qDebug() << __FUNCTION__;
    QDirIterator it("c:\\", {"qt"}, QDir::Dirs, QDirIterator::NoIteratorFlags);

    while (it.hasNext())
    {
        QString tmp = it.next();
        QDirIterator itfile(tmp, {"windeployqt.exe"}, QDir::Files, QDirIterator::Subdirectories);
        while (itfile.hasNext())
        {
            emit findQtDeployPath(itfile.next().replace("/","\\"));
        }
    }
    qDebug() << __FUNCTION__ << "done";
    looper->exec();
}

void BackEnd::onDoProcess(QString deployPath, QStringList arg)
{
    qDebug() << deployPath << arg;
    QFileInfo info(deployPath);
    m_process->setWorkingDirectory(info.absolutePath());
    m_process->start(deployPath,arg);
}

void BackEnd::readyReadStandardError()
{
    emit standardError(m_process->readAllStandardError());
}
void BackEnd::readyReadStandardOutput()
{
    emit standardOutput(m_process->readAllStandardOutput());
}
void BackEnd::finished(int exitCode, QProcess::ExitStatus exitStatus)
{
    qDebug() << __FUNCTION__ << exitCode << exitStatus;
}
