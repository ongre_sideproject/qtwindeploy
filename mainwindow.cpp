#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDirIterator>
#include <QDebug>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    checkRadio();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onFindQtDeployPath(QString path)
{
    ui->comboBox->addItem(path);
}

void MainWindow::on_pushButton_clicked()
{

}


void MainWindow::on_btnStart_clicked()
{
    QString exePath = ui->edPath->text();
    QFileInfo info(exePath);
    if(!info.isFile())
        return;

    QStringList arg;
    arg.append(exePath);



    emit doProcess(ui->comboBox->currentText(),arg);

}
void MainWindow::onStandardError(QString str)
{
    ui->textBrowser->append(str);
}

void MainWindow::onStandardOutput(QString str)
{
    ui->textBrowser->append(str);
}

void MainWindow::on_btnBrowser_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(
            this,
            "Select exe file for QT",
            "",
            tr("exe (*.exe)")
    );
    ui->edPath->setText(fileName.replace("/","\\"));
}

void MainWindow::on_btnQMLBrowser_clicked()
{
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::DirectoryOnly);
    dialog.setOption(QFileDialog::ShowDirsOnly, false);
    dialog.exec();
    ui->edQMLPath->setText(dialog.directory().path().replace("/","\\"));
    //ui->EdPath->setText(dialog.directory().path());
}

void MainWindow::on_btnQML_clicked()
{
    checkRadio();
}

void MainWindow::on_btnWidget_clicked()
{
    checkRadio();
}

void MainWindow::checkRadio()
{
    if(ui->btnQML->isChecked())
    {
        ui->edQMLPath->setEnabled(true);
        ui->btnQMLBrowser->setEnabled(true);
    }else
    {
        ui->edQMLPath->setEnabled(false);
        ui->btnQMLBrowser->setEnabled(false);
    }
}
