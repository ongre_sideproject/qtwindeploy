
# Overview
QT로 만들어진 실행파일을 개발환경과 다른 PC에서 실행할 수 있도록 라이브러리를 찾고, 현재 실행파일 위치에 복사하는 프로그램

# 개발환경
- QT

# Sequence
1. QT 설치 경로에 `windeployqt.exe`를 찾는다.
2. `windeployqt.exe`를 parameter와 함께 전달
